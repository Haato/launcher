# Launcher v2.8.5

Launcher is a software developed in python that lets you add multiple programs to a preset and open all the programs saved in the preset at the same time, saving your time from clicking in every icon twice to open the same programs.

The launcher has multiple useful functions, like saving the programs in a preset, changing themes, minimize to system tray, auto-update and much more other functions that will be implemented over time.

# Recent changes

+ Added balloon to show program's name when user hovers on the minimized icon
+ Added auto update and self update
+ Added check for updates button in options
+ Added PID detection, it'll be used in a future update
+ Added crash-detection, so if some critical issue happen, client will automatically look for the latest files
+ Added automatic detection for admin powers if program is being executed without admin
+ Added custom themes loader (Check ./themes/How_to_make_themes.md)

# Bug fixes

- Fixed bug where update button wasn't appearing on the screen;
