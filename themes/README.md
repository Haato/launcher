# Making a theme
The Launcher has a great way to make custom themes, where you can choose the background color, font color, font, type of font, font size, button colors, button images.

First of all, make a directory inside **root/themes/** with the same name you want for your theme (Must be a name without any capitalization).
Inside that folder, make a file called **theme_name.theme.json** and paste the following code inside it:

    {
	    "name" : "THEME_NAME",
	    "description" : "THEME_DESCRIPTION",
	    "images" : {},
	    "colors" : {
		    "bg" : null,
		    "font" : [null],
		    "fontPathButton" : [null],
		    "fg" : null,
		    "bgEntry" : null,
		    "activeBackground" : null,
		    "activeBackgroundEntry" : null,
		    "hoverButtonBorder" : null,
		    "checkButtonColor" : null 
	    }
    }

# Colors

 - **bg:** String with hex color or color name for the program background;
	 - e.g: 
		 `"bg" : "#ffffff"`
		or
		 `"bg" : "white"`
		 
 - **font:** Array with the Font name, font size, and any visual modifier* for the launcher texts (*optional);
	 - e.g:
		 `"font" : ["Arial", 13, "bold"]`
		 or
		 `"font" : ["Bahnschrift, 10]`
		 
- **fontPathButton:** Array with the font name, font size and any visual modifier* for the launcher path button (*optional);
	- e.g:
		`"fontPathButton" : ["Arial", 13, "bold"]`

- **fg:** Font color for the program main texts.
	- e.g: Same as **bg**

- **bgEntry:** Background color of entry buttons (text placeholders);
	- e.g: Same as **bg**

- **activeBackground:** Button background color when clicked;
	- e.g: Same as **bg**

- **activeBackgroundEntry:** Text placeholders background color when clicked;
	- e.g: Same as **bg**

- **hoverButtonBorder:** Button border color when hovering with the mouse;
	- e.g: Same as **bg**

- **checkButtonColor:** Check button tick color (options checkbutton);
	- e.g: Same as **bg**


# Images

If your custom theme has no button images or not enough images for all buttons, the program will load the default ones instead.
All images are 30x30, except the launch_button_image and update_now_image
- **add_new_program_image:** Add new program to current preset button image;
- **add_steam_game_image:** Add Steam game to current preset button image;
- **next_program_image:** Next program button image;
- **previous_program_image:** Previous program button image;
- **delete_program_image:** Delete program from current preset image;
- **
